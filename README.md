# README #

Generate local minima from input file or test inputs.

### Local minima task ###
Type "python3 find_minima.py --help" for help and options.

Most basic cmd examples for random test input:
python3 find_minima.py --puzzle_input puzzle_input.txt --log ./log

python3 find_minima.py --create_test_input ./test_input --puzzle_input ./test_input --log ./log

### Requirements ###
Please make sure that you have python version 3.