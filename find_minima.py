import logging, random, argparse
import sys
import os

class Puzzle(object):
    # not sure how deep the exceptions should go
    # e.g. not checking if the file is valid string based file or contains binary data
    def __init__(self, log_path=None):
        self.total = 0
        self.count = 0
        self.local_minima = []
        self.log_path = log_path
        if log_path != None:
            try:
                for handler in logging.root.handlers[:]:
                    logging.root.removeHandler(handler)
                logging.basicConfig(filename=log_path, filemode='w', level=logging.INFO,
                                    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
                                    datefmt='%Y-%m-%d %H:%M:%S')
            except:
                print("Please provide a valid log file path. Exiting program.")
                sys.exit()

    def extract_line(self, puzzle_input):
        # this function is for generating list of lines for 4 potential situations
        current_line_num = 1
        # Check if file exists
        if not os.path.exists(puzzle_input):
            print("Please provide a valid input file path. Exiting program.")
            sys.exit()
        with open(puzzle_input, "r") as puzzle_file:
            lines = []
            for line in puzzle_file:
                if line == "\n" or line == "\r\n":
                    continue
                lines.append(line.strip())
                if current_line_num == 2 and len(lines) == 2:
                    # find minima from the first line (first two lines are needed)
                    lines = self.find_minima(lines, "first", current_line_num)
                if len(lines) == 3:
                    # find minima from the middle lines (three lines are needed)
                    lines = self.find_minima(lines, "middle", current_line_num)
                current_line_num += 1
        if lines == []:
            print("Empty file")
            return
        if current_line_num == 2:
            # if the file has only one line
            self.find_minima(lines, "one_line_file", current_line_num)
        elif len(lines) < 3:
            # if its the end of the file (last two lines are needed)
            self.find_minima(lines, "end", current_line_num)
        print("The number of minima detected: " + str(self.count))
        print("The sum of the combined minimal values: " + str(self.total))
        return

    def find_minima(self, lines, line_pos, current_line_num):
        # keep track of the position of numbers in the line
        index = 0
        # adjust current line variable for logging & checking purposes
        current_line_num = current_line_num - 1
        if line_pos == "one_line_file":
            # I guess it can happen if you force it?
            line_len = len(lines[0])
            # only line, get length to avoid range errors
            for i in lines[0]:
                # always check if given input has non-numerical values
                add = self.check_is_digit(i, current_line_num)
                if add == False:
                    continue
                # in bigger projects group these in other functions such as "check_horizontal_values()"
                to_compare = []
                if index + 1 < line_len:
                    # check if we are at the edge or beginning to avoid range errors
                    to_compare.append(lines[0][index + 1])
                    # add horizontal neighbouring values to a list to compare
                if index - 1 > 0:
                    to_compare.append(lines[0][index - 1])
                for y in to_compare:
                    # always check if given input has non-numerical values
                    add = self.check_is_digit(y, current_line_num, log=False)
                    if i >= y:
                        # if "i" is equal or bigger than any neighbouring number, its not local minimum
                        add = False
                        break
                if add:
                    # is it still minimum if there's only one value in the file? Question for the philosophers.
                    self.add_minimum(i, current_line_num, index)
                index += 1
        if line_pos == "first":
            # first two lines of the file
            line_len = len(lines[0])
            next_line_len = len(lines[1])
            for i in lines[0]:
                add = self.check_is_digit(i, current_line_num)
                if add == False:
                    continue
                to_compare = []
                if index + 1 < line_len:
                    to_compare.append(lines[0][index + 1])
                if index - 1 > 0:
                    to_compare.append(lines[0][index - 1])
                if index < next_line_len:
                    # check if vertical neighbouring value exists
                    to_compare.append(lines[1][index])
                    # add vertical neighbouring values to a list to compare
                for y in to_compare:
                    add = self.check_is_digit(y, current_line_num, log=False)
                    if i >= y:
                        add = False
                        break
                if add:
                    self.add_minimum(i, current_line_num, index)
                index += 1
        if line_pos == "middle":
            # any consecutive 3 middle lines
            previous_line_len = len(lines[0])
            line_len = len(lines[1])
            next_line_len = len(lines[2])
            for i in lines[1]:
                add = self.check_is_digit(i, current_line_num)
                if add == False:
                    continue
                to_compare = []
                if index + 1 < line_len:
                    to_compare.append(lines[1][index + 1])
                if index - 1 > 0:
                    to_compare.append(lines[1][index - 1])
                if index < next_line_len:
                    to_compare.append(lines[2][index])
                if index < previous_line_len:
                    to_compare.append(lines[0][index])
                for y in to_compare:
                    add = self.check_is_digit(y, current_line_num, log=False)
                    if add == False:
                        break
                    if i >= y:
                        add = False
                        break
                if add:
                    self.add_minimum(i, current_line_num, index)
                index += 1
            del lines[0]
        #   delete the first line to make room for the next
        if line_pos == "end":
            # last two lines of the file
            previous_line_len = len(lines[0])
            line_len = len(lines[1])
            for i in lines[1]:
                add = self.check_is_digit(i, current_line_num)
                if add == False:
                    continue
                to_compare = []
                if index + 1 < line_len:
                    to_compare.append(lines[1][index + 1])
                if index - 1 > 0:
                    to_compare.append(lines[1][index - 1])
                if index < previous_line_len:
                    to_compare.append(lines[0][index])
                for y in to_compare:
                    add = self.check_is_digit(y, current_line_num, log=False)
                    if i >= y:
                        add = False
                        break
                if add:
                    self.add_minimum(i, current_line_num, index)
                index += 1
        return lines

    def add_minimum(self, i, current_line_num, index):
        self.total += int(i)
        self.count += 1
        self.log_position(i, current_line_num, index)

    def write_test_input(self, puzzle_path, line_amount, char_amount):
        with open(puzzle_path, "w") as input_file:
            for i in range(0, line_amount):
                character_amount = random.randint(0, char_amount)
                line = ""
                if character_amount != 0:
                    for y in range(0, character_amount):
                        line = line + str(random.randint(0, 9))
                line = line + "\n"
                input_file.write(line)

    def check_is_digit(self, num, current_line_num, log=True):
        add = True
        if not num.isdigit():
            add = False
            try:
                if log:
                    logging.error("Character %s not digit at line %s" % (str(num), str(current_line_num)))
            except:
                logging.error("Unknown character at line %s" % (str(current_line_num)))
        return add

    def log_position(self, char, current_line_num, index):
        logging.info("Character %s is a local minimum at line %s at %s" % (str(char), str(current_line_num), str(index +1)))


def run_puzzle(puzzle_input, log, create_test_input, test_line_num, test_char_amount):
    puzzle = Puzzle(log)
    if create_test_input != None:
        try:
            open(create_test_input, "w")
            os.remove(create_test_input)
        except:
            print("Please provide a valid test input file path. Exiting program.")
            sys.exit()
        if not str(test_line_num).isdigit() or not str(test_char_amount).isdigit():
            print("Please provide a integers for line and character amounts. Exiting program.")
            sys.exit()
        puzzle.write_test_input(create_test_input, int(test_line_num), int(test_char_amount))
        if puzzle_input == create_test_input:
            puzzle.extract_line(puzzle_input)
            return
    if puzzle_input == None:
        print("Please provide a valid input file path. Exiting program.")
        sys.exit()
    puzzle.extract_line(puzzle_input)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="""Mini project for finding local minima""")
    parser.add_argument("--puzzle_input", help="Enter the file path of the matrix input", default=None)
    parser.add_argument("--log", help="Enter log file path if you want to track logs", default=None)
    parser.add_argument("--create_test_input", help="Enter the file path of the random test matrix input", default=None)
    parser.add_argument("--test_line_num", help="Enter desired test line number", default=40)
    parser.add_argument("--test_char_amount", help="Enter desired test character number per line", default=100)
    args = parser.parse_args()

    run_puzzle(puzzle_input=args.puzzle_input, log=args.log, create_test_input=args.create_test_input, test_line_num=args.test_line_num, test_char_amount=args.test_char_amount)
